 /*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package proyecto;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

/**
 *
 * @author kmilo
 */
public class Principal extends javax.swing.JFrame {

    String lectura = "";
    String path = "";
    File f;

    /**
     * Creates new form Principal
     */
    public Principal() {
        initComponents();
        bienvenida();
        btnGuardar.setVisible(false);
    }

    public void bienvenida() {
        JOptionPane.showMessageDialog(rootPane,
                "Bienvenidos a nuestro programa, \n"
                + "En nuestro programa podras realizar la inscripcion "
                + "para el museo central de UCompensar "
                + "\n\n Este programa fue realizado por Vanessa Ruiz"
        );
    }

    public String solicitudDatosE() {
        String informacion_datos = "";
        int num_visitantes;
        int edad = 0;
        int valorTotalMuseo = 0;
        int valorMuseo = 0;
        int menores_edad = 0;
        int mayor_edad = 0;
        String nombres[];
        String datos[][];
        num_visitantes = Integer.parseInt(JOptionPane.showInputDialog(""
                + "Digite el numero de visitantes que desea registrar"));
        nombres = new String[num_visitantes];
        datos = new String[num_visitantes][4];
        informacion_datos = informacion_datos + "Nombre" + "\t" + "Identificación" + "\t" + "Edad" + "\t" + "Afiliación" + "\t" + "Categoria" + "\t" + "Valor museo" + "\n";
        for (int i = 0; i < num_visitantes; i++) {
            nombres[i] = JOptionPane.showInputDialog(""
                    + "Digite el nombre del visitante # " + (i + 1));
            datos[i][0] = JOptionPane.showInputDialog(""
                    + "Digite el numero de identificacion del visitante # " + (i + 1));
            datos[i][1] = JOptionPane.showInputDialog(""
                    + "Digite la edad del visitante # " + (i + 1));
            edad = Integer.parseInt(datos[i][1]);
            datos[i][2] = JOptionPane.showInputDialog(""
                    + "Digite si el visitante # " + (i + 1) + " esta afiliado a la caja de compensación");
            if ("si".equals(datos[i][2])) {
                datos[i][3] = JOptionPane.showInputDialog(""
                        + "Digite la categoria del visitante # " + (i + 1));
            }
            if ("no".equals(datos[i][2])) {
                datos[i][3] = "No Aplica";
            }
            if (edad < 18) {
                menores_edad = menores_edad + 1;
            } else {
                mayor_edad = mayor_edad + 1;
            }
            if (edad < 18) {
                valorMuseo = 5000;
            } else {
                if (edad >= 18 && "no".equals(datos[i][2])) {
                    valorMuseo = 30000;
                } else {
                    if ("si".equals(datos[i][2]) && "a".equals(datos[i][3])) {
                        valorMuseo = 25500;
                    } else {
                        if ("si".equals(datos[i][2]) && "b".equals(datos[i][3])) {
                            valorMuseo = 21000;
                        } else {
                            if ("si".equals(datos[i][2]) && "c".equals(datos[i][3])) {
                                valorMuseo = 15000;
                            }
                        }
                    }
                }
            }
            valorTotalMuseo = valorTotalMuseo + valorMuseo;
            informacion_datos = informacion_datos + nombres[i] + "\t" + datos[i][0] + "\t" + edad + "\t" + datos[i][2] + "\t" + datos[i][3] + "\t" + valorMuseo + "\n";
        }
        informacion_datos = informacion_datos + "\n";
        informacion_datos = informacion_datos + "Menores de edad" + "\t" + menores_edad + "\n";
        informacion_datos = informacion_datos + "Mayores de edad" + "\t" + mayor_edad + "\n";
        informacion_datos = informacion_datos + "Valor total a pagar" + "\t" + valorTotalMuseo + "\n";
        concatenar(informacion_datos);
        return informacion_datos;
    }

    public String mostrar(String lista_estudiante) {
        String datos_totales;
        datos_totales = txtADatos.getText() + lista_estudiante;

        return datos_totales;
    }

    public void mostrarDatos() {
        try {
            FileReader lectorArchivo = new FileReader(f);
            BufferedReader br = new BufferedReader(lectorArchivo);
            String aux = "";
            while (true) {
                aux = br.readLine();
                if (aux != null) {
                    lectura = lectura + aux + "\n";
                } else {
                    break;
                }
                br.close();
                lectorArchivo.close();
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(rootPane, "Error" + e.getMessage());
        }
    }

    public void concatenar(String texto) {
        FileWriter EscritorArchivos;
        try {
            String temporal = lectura + texto;
            EscritorArchivos = new FileWriter(f);
            BufferedWriter bw = new BufferedWriter(EscritorArchivos);
            PrintWriter salida = new PrintWriter(bw);
            salida.write(temporal);
             JOptionPane.showMessageDialog(rootPane, "Se registraron los datos");
             salida.close();
             bw.close();
             EscritorArchivos.close();
        } catch (Exception e) {
             JOptionPane.showMessageDialog(rootPane, "Error" + e.getMessage());
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        btnGuardar = new javax.swing.JButton();
        btnBuscar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtADatos = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(255, 51, 51));
        jPanel1.setForeground(new java.awt.Color(102, 255, 102));

        jLabel1.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 0, 0));
        jLabel1.setText("Inscripciones al museo central de UComensar");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(188, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 339, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(183, 183, 183))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(38, 38, 38)
                .addComponent(jLabel1)
                .addContainerGap(46, Short.MAX_VALUE))
        );

        jLabel2.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel2.setText("Visitantes Inscritos");

        btnGuardar.setText("Crear Inscripción");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });

        btnBuscar.setText("Buscar archivo");
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });

        txtADatos.setEditable(false);
        txtADatos.setColumns(20);
        txtADatos.setRows(5);
        jScrollPane1.setViewportView(txtADatos);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addComponent(jScrollPane1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnGuardar, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(16, 16, 16))
            .addGroup(layout.createSequentialGroup()
                .addGap(198, 198, 198)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel2)
                        .addGap(12, 12, 12)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 16, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnGuardar)
                        .addGap(48, 48, 48)
                        .addComponent(btnBuscar)
                        .addGap(57, 57, 57))))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed

        String datos = solicitudDatosE();
        String datos_totales = mostrar(datos);
        txtADatos.setText(datos_totales);
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed
        JFileChooser jf = new JFileChooser();
        jf.showOpenDialog(jPanel1);
        try {
            path = jf.getSelectedFile().getAbsolutePath();
            f = new File(path);
            mostrarDatos();
        } catch (Exception e) {
            //JOptionPane.showMessageDialog(rootPane, "Error" + e.getMessage());
        }
        txtADatos.setText(lectura);
        btnGuardar.setVisible(true);
    }//GEN-LAST:event_btnBuscarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Principal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea txtADatos;
    // End of variables declaration//GEN-END:variables
}
